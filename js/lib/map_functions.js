/* TalkingStory Map functions JS */

function render_map(div_id){
  //console.log(GlobalContents.maps[div_id]);
  $('#'+div_id).empty();
  var baselayers = GlobalContents.maps[div_id]['baselayers'];
  var overlayers = GlobalContents.maps[div_id]['overlayers'];
  var markers = GlobalContents.maps[div_id]['markers'];
  var bounds = GlobalContents.maps[div_id]['bounds'];
  GlobalContents.maps[div_id]['map'] = new MyMap(div_id,baselayers,overlayers,markers);
  GlobalContents.maps[div_id]['map'].createMap(0,0,1);
  GlobalContents.maps[div_id]['map'].zoomToBB(bounds);
}

function populate_map_form(div_id,map_id=null){

  var mapObj2render;

  /* if there is already a div then get the map id form the div otherwise use directly the map id from GlobalMaps */
  if ( typeof GlobalContents.maps[div_id] != 'undefined' && map_id==null){
      var map_id = null;
      $.each(GlobalMaps,function(key,obj){
          if (obj.div_id == div_id){
            map_id = key;
          } 
      });
      mapObj2render = GlobalContents.maps[div_id];
  } else if ( map_id != null){
      mapObj2render = GlobalMaps[map_id].map;
  }  else {
      mapObj2render = null;
  }

 if (  mapObj2render != null ) {
   console.log('Map already defined, loading data slide: '+div_id);
   
   /* Loading General infos */
   var general_info = mapObj2render.general_info;
   $.each(Object.keys(general_info),function(idx,key){
      $('#form_map .form_general[name="'+key+'"]').val(general_info[key]);
      //..TODO manage different object type case here
   });
   
   /* Loading Baselayers from GlobalContents.maps variable */
   var baselayers = mapObj2render['baselayers'];
   $.each(baselayers,function(idx,obj){
      if( obj.hasOwnProperty('default') && obj.default ){
          
      }
      switch (obj.source){
            
            case "default":
                $('#CheckOSM').prop('checked',true);
                break;
            case "default_satellite":
                $('#CheckSatellite').prop('checked',true);
                break;
            case "mapbox":
                  $(Object.keys(obj)).each(function(idx,key){
                      if (key == 'opt'){
                        $(Object.keys(obj['opt'])).each(function(idx,key){
                            $('#form_map .form_baselayers_mapbox[name="opt-'+key+'"]').val(obj['opt'][key]);  
                        });  
                      }else{
                        $('#form_map .form_baselayers_mapbox[name="'+key+'"]').val(obj[key]);
                      }
                  });
                break;
            case "generic_wms":
                  $(Object.keys(obj)).each(function(idx,key){
                      if (key == 'opt'){
                        $(Object.keys(obj['opt'])).each(function(idx,key){
                            $('#form_map .form_baselayers_wms[name="opt-'+key+'"]').val(obj['opt'][key]);  
                        });  
                      }else{
                        $('#form_map .form_baselayers_wms[name="'+key+'"]').val(obj[key]);
                      }
                  });
                break;
      }
   });
   
    /* Loading Overlayers from GlobalContents.maps variable */
    var overlayers = mapObj2render['overlayers'];        
    $.each(overlayers,function(idx,obj){
        
         var target_id = obj.target_id;
         
         if ($(target_id).length == 0){
               var group_id_div = target_id.split('-')[0];
               var group_id_first_elem = group_id_div+'-1';
               var g_layer_clone = $(group_id_first_elem).clone().attr('id',target_id.replace("#",""));
               $(group_id_div).append( g_layer_clone );
               
         }
        
         switch (obj.type){
             case "wms":
                 $(Object.keys(obj)).each(function(idx,key){
                      if (key == 'opt'){
                        $(Object.keys(obj['opt'])).each(function(idx,key){
                            $(target_id+' .form_overlayers_wms[name="opt-'+key+'"]').val(obj['opt'][key]);  
                        });  
                      }else if(key=='visibility'){
                        if (obj[key]){
                            $(target_id+' .form_overlayers_wms[name="'+key+'"]').prop('checked',true);  
                        }
                      }else{
                        $(target_id+' .form_overlayers_wms[name="'+key+'"]').val(obj[key]);
                      }
                  });
                 break;
             case "geojson":
             case "prj_geojson":
                 $(Object.keys(obj)).each(function(idx,key){
                      if (key == 'opt'){
                        $(Object.keys(obj['opt'])).each(function(idx,key){
                            if (key=="popup_fields"){
                                if ( typeof(obj['opt'][key]) == 'object' ){
                                    $(target_id+' .form_overlayers_gjson[name="opt-'+key+'"]').val( JSON.stringify( obj['opt'][key] ) );
                                }else{
                                    $(target_id+' .form_overlayers_gjson[name="opt-'+key+'"]').val(obj['opt'][key]);                                  
                                }
                            }else{
                                $(target_id+' .form_overlayers_gjson[name="opt-'+key+'"]').val(obj['opt'][key]);  
                            }
                        });  
                      }else if(key=='visibility'){
                        if (obj[key]){
                            $(target_id+' .form_overlayers_gjson[name="'+key+'"]').prop('checked',true);  
                        }
                      }else{
                        $(target_id+' .form_overlayers_gjson[name="'+key+'"]').val(obj[key]);
                      }
                  });
                 break;
         }   
    });
   
   var mks = mapObj2render['markers'];
   var bounds = mapObj2render['bounds'];

    $('#map_preview').show();
    $('#map_preview').css({position:'relative',width:'-webkit-fill-available'});
    $('#map_default_bbox').show(); 
    $('#map_idx_title').show();
    $('#map_idx_title').empty().append('Map: <h3 id="map_title" mapid="'+map_id+'">'+general_info.map_name+'</h3>');

    if (mapPreview == null){
      setTimeout(function(){
        mapPreview = new MyMap(map_preview_id,baselayers,overlayers,markers_list=mks);
        mapPreview.createMap(0,0,1,map_editable=true);    
        mapPreview.zoomToBB(bounds);
         // EDITABLE MARKERS
        editable_markers_preview(mapPreview,map_id);
      },1000);
    }else{
      console.log('Restoring Map data...');
      mapPreview.map.remove();
      setTimeout(function(){
           mapPreview = new MyMap(map_preview_id,baselayers,overlayers,markers_list=mks);
           mapPreview.createMap(0,0,1,map_editable=true);    
           mapPreview.zoomToBB(bounds);
         // EDITABLE MARKERS
           editable_markers_preview(mapPreview,map_id);
      },1000);
    }   
    
 }else{
   console.log('ERROR Missing Both Map div & Map ID');
 }
}

function arcgis_map_extent(){
   $('#arcgis_extent_map, #arcgis_extent').toggleClass('arcgis_extent_show');
   if ($('#arcgis_extent_map').html().length == 0){
       var basel = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
                maxZoom: 16,
                attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'
            });  
      var map = L.map('arcgis_extent_map').setView([0, 0], 1).addLayer(basel);
      map.on('moveend', function(e) {
       var bounds = map.getBounds();
       var b_arr = [ bounds._southWest.lng, bounds._southWest.lat, bounds._northEast.lng, bounds._northEast.lat ];   
       $.each(b_arr,function(idx,value){
         $($('.input-arcgis-extent')[idx]).val( value );
       });
        arcgis_replace_extents();        
      });     
   }
}

function editable_markers_preview(mapPreview,mapid){
  
  var marker_template = '';
  
  console.log(mapPreview.markers_list);
//  var markers = [];
//        var markers_feature = L.featureGroup([]);
//        mapPreview.map.addLayer( markers_feature );
//        var drawControl = new L.Control.Draw({
//            edit: {
//                //featureGroup: drawnItems
//                 featureGroup: markers_feature,
//                 remove: true
//            },
//          draw: {
//                polygon: false,
//            circle:false,
//            line:false,
//            polyline:false,
//            rectangle:false,
//                marker: true
//            },
//          position: 'bottomright'
//        });
//        //mapPreview.map.addControl(drawControl);
//        mapPreview.map.addControl(drawControl);
       
       mapPreview.map.on(L.Draw.Event.CREATED, function (event) {
        var layer = event.layer;
        var new_key = Object.keys(GlobalMaps[mapid].map.markers).length;
       // drawnItems.addLayer(layer);
        var deafault_popup = '<div id="this-marker-editable_'+new_key+'" marker="'+new_key+'" mapid="'+mapid+'">New Marker</div>';
        layer.bindPopup(deafault_popup);         
         mapPreview.markers_f.addLayer(layer);         
         //markers.push( {lat: layer._latlng.lat, lon: layer._latlng.lng, popup: deafault_popup} );
          console.log(mapid);
          console.log(GlobalMaps);
         console.log(GlobalMaps[mapid].map);
         GlobalMaps[mapid].map.markers[new_key] = {lat: layer._latlng.lat, lon: layer._latlng.lng, popup: deafault_popup};
         console.log(GlobalMaps);
       });
  
       mapPreview.map.on(L.Draw.Event.EDITED, function (event) {
          alert('Marker Edit event >> TODO');
       });

        mapPreview.map.on(L.Draw.Event.DELETED, function (event) {
          alert('Marker Delete event >> TODO');
       }); 
  
        mapPreview.map.on('popupopen', function (e) {
//           //e.popup._source.setIcon(new L.Icon({ iconUrl: 'https://www.hallofseries.com/wp-content/uploads/2018/05/Boris.png' }));
//          //CKEDITOR.inline( 'this-marker-editable' );        
//          //$('#this-marker-editable').on('change paste keyup',function(){
//          //  e.popup._source._popup._content = $('#this-marker-editable')[0].outerHTML;
//          //});e.popup._source._popup._content
          console.log(e.popup._leaflet_id);
          var popup_id = $(e.popup._source._popup._content)[0].id;
          var mapid = $(e.popup._source._popup._content)[0].getAttribute('mapid');
          var popup_content = e.popup._source._popup._content;
          $('#'+popup_id).append('<br><div class="marker-editable-edit-button">\
 <button id="edit_marker_text" type="button" class="btn btn-sm btn-dark" \
          data-toggle="modal" data-target="#marker-text">Edit</button></div>');
          //$('#marker_popup_text').html(popup_content);
          CKEDITOR.instances.popup_editor.setData(popup_content);
          var marker_id_map = popup_id.split('_')[1] + '-' + mapid + '-' + e.popup._leaflet_id;
          $('#marker_popup_text').parent().find('.modal-header code').html(marker_id_map);          
        });
  
  //return markers;

}

function save_popup_marker(){
  var marker_header = $('#marker_header').val();
  var marker_id = $('#marker-text code').html().split('-')[0];
  var map_id = $('#marker-text code').html().split('-')[1];
  var marker_leaflet_id = $('#marker-text code').html().split('-')[2];
  var content = CKEDITOR.instances.popup_editor.getData();
  
  var popup_content = '<b id="marker-header-title">'+marker_header+'</b><hr></hr>'+ content;
  GlobalMaps[map_id].map.markers[marker_id].popup = popup_content;
  GlobalMaps[map_id].map.markers[marker_id]["title"] = marker_header;
  mapPreview.map._layers[marker_leaflet_id]._content = popup_content;
  $('#this-marker-editable_'+marker_id).html(popup_content);
  alert('TODO.. save marker popup content:\
'+content+'\
...to preview marker popup');
  
}